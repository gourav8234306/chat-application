from django.db import models


# Create your models here.

from django.db import models
from django.contrib.auth.hashers import check_password
class User(models.Model):
    username = models.CharField(max_length=20, unique=True)
    password = models.CharField(max_length=128)
    email = models.EmailField(unique=True)
    contact_number = models.BigIntegerField(unique=True)
    def __str__(self):
        return self.username
    def check_password(self, raw_password):
        return check_password(raw_password, self.password)

