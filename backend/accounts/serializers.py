from rest_framework import serializers
from .models import User
from django.contrib.auth.hashers import make_password

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'email', 'contact_number']
        extra_kwargs = {
            'password': {'write_only': True}, 
            'email': {'required': True},    
            'contact_number': {'required': True} 
        }

    def validate(self, data):
        # Check if the username already exists in the database
        existing_username = User.objects.filter(username=data.get('username')).exists()
        if existing_username:
            raise serializers.ValidationError("Username already exists.")

        # Check if the email already exists in the database
        existing_email = User.objects.filter(email=data.get('email')).exists()
        if existing_email:
            raise serializers.ValidationError("Email already exists.")

        return data
    def create(self, validated_data):
        # Hash the password before saving it
        validated_data['password'] = make_password(validated_data['password'])
        return super().create(validated_data)
